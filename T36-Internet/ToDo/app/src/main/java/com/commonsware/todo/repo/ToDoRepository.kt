package com.commonsware.todo.repo

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

enum class FilterMode { ALL, OUTSTANDING, COMPLETED }

class ToDoRepository(
  private val store: ToDoEntity.Store,
  private val remoteDataSource: ToDoRemoteDataSource
) {
  fun items(filterMode: FilterMode = FilterMode.ALL): Flow<List<ToDoModel>> =
    filteredEntities(filterMode).map { all -> all.map { it.toModel() } }

  private fun filteredEntities(filterMode: FilterMode) = when (filterMode) {
    FilterMode.ALL -> store.all()
    FilterMode.OUTSTANDING -> store.filtered(isCompleted = false)
    FilterMode.COMPLETED -> store.filtered(isCompleted = true)
  }

  fun find(id: String): Flow<ToDoModel?> = store.find(id).map { it?.toModel() }

  suspend fun save(model: ToDoModel) {
    store.save(ToDoEntity(model))
  }

  suspend fun delete(model: ToDoModel) {
    store.delete(ToDoEntity(model))
  }

  suspend fun importItems(url: String) {
    store.importItems(remoteDataSource.load(url).map { it.toEntity() })
  }
}