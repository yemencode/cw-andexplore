package com.commonsware.todo.ui.roster

import android.net.Uri
import androidx.lifecycle.*
import com.commonsware.todo.repo.FilterMode
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import com.commonsware.todo.report.RosterReport
import com.commonsware.todo.ui.util.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class RosterViewState(
  val items: List<ToDoModel> = listOf(),
  val filterMode: FilterMode = FilterMode.ALL
)

sealed class Nav {
  data class ViewReport(val doc: Uri) : Nav()
}

class RosterMotor(private val repo: ToDoRepository, private val report: RosterReport) : ViewModel() {
  private val _states = MediatorLiveData<RosterViewState>()
  val states: LiveData<RosterViewState> = _states
  private var lastSource: LiveData<RosterViewState>? = null
  private val _navEvents = MutableLiveData<Event<Nav>>()
  val navEvents: LiveData<Event<Nav>> = _navEvents

  init {
    load(FilterMode.ALL)
  }

  fun load(filterMode: FilterMode) {
    lastSource?.let { _states.removeSource(it) }

    val items =
      repo.items(filterMode).map { RosterViewState(it, filterMode) }.asLiveData()

    _states.addSource(items) { viewstate ->
      _states.value = viewstate
    }

    lastSource = items
  }

  fun save(model: ToDoModel) {
    viewModelScope.launch(Dispatchers.Main) {
      repo.save(model)
    }
  }

  fun saveReport(doc: Uri) {
    viewModelScope.launch(Dispatchers.Main) {
      _states.value?.let { report.generate(it.items, doc) }
      _navEvents.postValue(Event(Nav.ViewReport(doc)))
    }
  }
}