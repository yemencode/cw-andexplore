package com.commonsware.todo.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.commonsware.todo.MainDispatcherRule
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SingleModelMotorTest {
  @get:Rule
  val instantTaskExecutorRule = InstantTaskExecutorRule()

  @get:Rule
  val mainDispatcherRule = MainDispatcherRule(paused = true)

  private val testModel = ToDoModel("this is a test")
  private val repo: ToDoRepository = mock()
  private lateinit var underTest: SingleModelMotor

  @Before
  fun setUp() {
    When calling repo.find(testModel.id) itReturns MutableLiveData<ToDoModel>(testModel)

    underTest = SingleModelMotor(repo, testModel.id)
  }

  @Test
  fun `initial state`() {
    underTest.states.test().awaitValue().assertValue { it.item == testModel }
  }

  @Test
  fun `actions pass through to repo`() {
    val replacement = testModel.copy("whatevs")

    underTest.save(replacement)
    mainDispatcherRule.dispatcher.runCurrent()

    runBlocking { Verify on repo that repo.save(replacement) was called }

    underTest.delete(replacement)
    mainDispatcherRule.dispatcher.runCurrent()

    runBlocking { Verify on repo that repo.delete(replacement) was called }
  }
}