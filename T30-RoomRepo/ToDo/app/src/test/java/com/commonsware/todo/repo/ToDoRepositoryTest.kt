package com.commonsware.todo.repo

import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBeEmpty
import org.amshove.kluent.shouldContainSame
import org.amshove.kluent.shouldEqual
import org.junit.Test

class ToDoRepositoryTest {
  private val underTest = ToDoRepository(TestStore())

  @Test
  fun `can add items`() = runBlockingTest {
    val testModel = ToDoModel("test model")
    val results = mutableListOf<List<ToDoModel>>()

    val itemsJob = launch {
      underTest.items().collect { results.add(it) }
    }

    results[0].shouldBeEmpty()

    underTest.save(testModel)

    results[1] shouldContainSame listOf(testModel)

    underTest.find(testModel.id).first() shouldEqual testModel

    itemsJob.cancel()
  }

  @Test
  fun `can modify items`() = runBlockingTest {
    val testModel = ToDoModel("test model")
    val replacement = testModel.copy(notes = "This is the replacement")
    val results = mutableListOf<List<ToDoModel>>()

    val itemsJob = launch {
      underTest.items().collect { results.add(it) }
    }

    results[0].shouldBeEmpty()

    underTest.save(testModel)

    results[1] shouldContainSame listOf(testModel)

    underTest.save(replacement)

    results[2] shouldContainSame listOf(replacement)

    itemsJob.cancel()
  }

  @Test
  fun `can remove items`() = runBlockingTest {
    val testModel = ToDoModel("test model")
    val results = mutableListOf<List<ToDoModel>>()

    val itemsJob = launch {
      underTest.items().collect { results.add(it) }
    }

    results[0].shouldBeEmpty()

    underTest.save(testModel)

    results[1] shouldContainSame listOf(testModel)

    underTest.delete(testModel)

    results[2].shouldBeEmpty()

    itemsJob.cancel()
  }
}

class TestStore : ToDoEntity.Store {
  private val items = mutableMapOf<String, ToDoEntity>()
  private val channel = ConflatedBroadcastChannel(items.values.toList())

  override fun all(): Flow<List<ToDoEntity>> = channel.asFlow()

  override suspend fun save(vararg entities: ToDoEntity) {
    entities.forEach { entity ->
      items[entity.id] = entity
    }

    channel.send(items.values.toList())
  }

  override suspend fun delete(vararg entities: ToDoEntity) {
    entities.forEach { entity ->
      items.remove(entity.id)
    }

    channel.send(items.values.toList())
  }

  override fun find(modelId: String): Flow<ToDoEntity> =
    flowOf(items[modelId]!!)
}
